/* Window Load functions */

$(window).load(function(){
    setTimeout(function(){

    });
});


$(document).ready(function(){
    
    $('header .login').click(function(){
       $('.login_popup').addClass('active');
    });
     $('.popup .inner .forgot').click(function(){
         $('.login_popup').removeClass('active');
       $('.password_popup').addClass('active');
    });
     $('.popup .inner>img').click(function(){
       $('.login_popup').removeClass('active');
    });

    if($(window).width() > 1024){
        $('header .right_col nav>ul>li').hover(
            function () {
                $(this).find('.drop').addClass('active');
                $(this).find('.drop').next().addClass('active');
            }, 
            function () {
                $(this).find('.drop').removeClass('active');
                $(this).find('.drop').next().removeClass('active');
            }
        );

    }else{
        $('header .right_col ul li .dropdown').slideUp();
        $('header .right_col .drop').click(function(){
            $(this).toggleClass('active');
            $(this).next().slideToggle();
        });

    }
    $(window).scroll(function(){
        var sticky = $('header'),
            scroll = $(window).scrollTop();
        if (scroll >= 100) sticky.addClass('fixed');
        else sticky.removeClass('fixed');
    });

    $('.hamburger').click(function(){
        $(this).toggleClass('active'); 
        $(' header .right_col').toggleClass('active'); 
    });

});

$(window).resize(function(){

});

